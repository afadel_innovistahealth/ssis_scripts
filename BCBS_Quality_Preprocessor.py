import pandas as pd
import os
import os.path
import sys
import shutil
import time
import numpy as np
import openpyxl as pyxl
import argparse
###BCBS Parser for ACO Quality Data Files

###This function tells us how many rows we need to skip
###read the excel file open the excel file read line by line and return the amount of rows we are to skip
###Look for included in numerator
def RowsToSkip(wbName,SheetName):
	print "Rows To Skip has been called"
	###Debug Print
	#print wbName
	#print SheetName
	wb = pyxl.load_workbook(wbName)
 
	ws = wb[SheetName]
	
	if ws["A8"].value == "Included in Numerator":
		print "7"
		return 7
	elif ws["A9"].value == "Included in Numerator":
		print "8"
		return 8
	elif ws["A10"].value == "Included in Numerator":
		print "9"
		return 9
	elif ws["A11"].value == "Included in Numerator":
		print "10"
		return 10
	elif ws["12"].value == "Included in Numerator":
		print "11"
		return 11
	else:
		print "No valid amount of rows to skip"
		###Raise Exception as headers cannot be found
		raise Exception("File not formatted correctly cannot find Headers")
		
###Begin the Main Function
###This function parses the file and creates a pipe delimited extract File
def CleanBCBSFile(workbook,extractFile):
	CurrentTime = time.strftime("%Y-%m-%d")	
	CurrentTime = str(CurrentTime)
	
	###Debug Hard Coded
	#filename = "ACO_ADHOC.MONTHLYGAPSINCAREDETAIL.INNOVISTA_HRAC.20161207.095011.xlsx"
	
	filename = str(workbook)
	
	###Getting our setup information
	wbName = filename
	wb = pyxl.load_workbook(wbName)
	ws = wb["D-Use of Imaging Studies for Lo"]
	
	###Find Correct value for claims through date and company
	###Check most common cell for setup information
	setupInfo = ws['E3'].value
	###Loop through if value cannot be found
	setupCheck = 0
	while setupInfo is None:
		print "No Value"
		###Check individual cells for keyword
		letters = ["F","E","A","B","C","D","G"]
		for x in letters:
			for cellLevel in range(5):
				#print setupCheck
				cellToCheck = str(letters[setupCheck] + str(cellLevel + 1))
				###Debug Print
				#print cellToCheck
				setupInfo = ws[cellToCheck].value
				if setupInfo is not None:
					break
				
		if setupCheck > 30:
			raise Exception("Claims Through Date cannot be found")
				
		setupCheck = setupCheck + 1	
		
	setupInfo = setupInfo.split('\n')
	
	###Debug Print
	#print setupInfo
	###Parse setupInfo to get claims through date
	for x in setupInfo:
		if "Claims" and "Paid" in x:
			print x
			claimsDate = x.split(':')
			claimsThrough = claimsDate[1]
			###Strip all white space
			claimsthroughdate = claimsThrough.strip()
			print claimsThrough
		elif "ACO" and "#" in x:
			company = str(x)
			print x

	###Begin Parsing of File
	#Look for company name in filename
	if "UPSA" in filename:
		Company = "UPSA"		
	elif "HRAC" in filename:
		Company = "HRAC"
	elif "CATALYST" in filename:
		Company = "CATALYST"
	elif "SPAC" in filename:
		Company = "SPAC"
	elif "ETRA" in filename:
		Company = "ETRA"
	elif "IPP" in filename:
		Company = "IPP"	
	elif "KANE" in filename:
		Company = "KANE"
	elif "FOXVALLEY" in filename:
		Company = "FOXVALLEY"
	elif "SEVENFLAGS" in filename:
		Company = "SEVENFLAGS"
	elif "HILLCOUNTRY" in filename:
		Company = "HILLCOUNTRY"
	elif "OSLER" in filename:
		Company = "OSLER"
	elif "PAACO" in filename:
		Company = "PAACO"
	elif "UNIFIED" in filename:
		Company = "UPN"
	else:
		Company = "Company Not Found"

	###Read file into dataframe		
	x1 = pd.ExcelFile(filename)
	###Parse the measures based on sheet_name due to inconsistency of header information
	for x in x1.sheet_names:
		if x[2:] in 'Appropriate Testing for Children With Pharyngitis':	
			skipNumber = RowsToSkip(filename,x)
			df = x1.parse(x,skiprows = skipNumber)
			df['Measure'] = 'Appropriate Testing for Children With Pharyngitis'
			df['ClaimsthroughDate'] =  claimsthroughdate
			if "Exclusion" not in df.columns:
				df["Exclusion"] = ""
				
		if x[2:] in 'Appropriate Treatment for Children With Upper Respiratory Infection':  
			skipNumber = RowsToSkip(filename,x)
			df1 = x1.parse(x,skiprows = skipNumber)
			df1['Measure'] = 'Appropriate Treatment for Children With Upper Respiratory Infection'
			df1['ClaimsthroughDate'] =  claimsthroughdate
			if "Exclusion" not in df1.columns:
				df1["Exclusion"] = ""

		if x[2:] in 'Avoidance of Antibiotic Treatment in Adults With Acute Bronchitis':
			skipNumber = RowsToSkip(filename,x)
			df2 = x1.parse(x,skiprows = skipNumber)
			df2['Measure'] = 'Avoidance of Antibiotic Treatment in Adults With Acute Bronchitis'
			df2['ClaimsthroughDate'] =  claimsthroughdate
			if "Exclusion" not in df2.columns:
				df2["Exclusion"] = ""
				
		if x[2:] in 'Breast Cancer Screening':
			skipNumber = RowsToSkip(filename,x)
			df3 = x1.parse(x,skiprows = skipNumber)
			df3['Measure'] = 'Breast Cancer Screening'
			df3['ClaimsthroughDate'] =  claimsthroughdate
			if "Exclusion" not in df3.columns:
				df3["Exclusion"] = ""

		if x[2:] in 'Cervical Cancer Screening':
			skipNumber = RowsToSkip(filename,x)
			df4 = x1.parse(x,skiprows = skipNumber)	
			df4['Measure'] = 'Cervical Cancer Screening'
			df4['ClaimsthroughDate'] =  claimsthroughdate
			if "Exclusion" not in df4.columns:
				df4["Exclusion"] = ""
			
		if x[2:] in 'D-Childhood Immunization Status':		
			skipNumber = RowsToSkip(filename,x)
			df5 = x1.parse(x,skiprows = skipNumber)
			df5['Measure'] = 'Childhood Immunization Status - MMR'
			df5['ClaimsthroughDate'] =  claimsthroughdate
			if "Exclusion" not in df5.columns:
				df5["Exclusion"] = ""
				
		if x[2:] in 'Colorectal Cancer Screening':	
			skipNumber = RowsToSkip(filename,x)
			df6 = x1.parse(x,skiprows = skipNumber)
			df6['Measure'] = 'Colorectal Cancer Screening'	
			df6['ClaimsthroughDate'] =  claimsthroughdate
			if "Exclusion" not in df6.columns:
				df6["Exclusion"] = ""
			
		if x[2:] in	'D-Comprehensive Diabetes Care -':
			skipNumber = RowsToSkip(filename,x)
			df7 = x1.parse(x,skiprows = skipNumber)
			df7['Measure'] = 'Comprehensive Diabetes Care - Blood Pressure Control < 140/90'
			df7['ClaimsthroughDate'] =  claimsthroughdate
			if "Exclusion" not in df7.columns:
				df7["Exclusion"] = ""
				
		if x[2:] in 'D-Comprehensive Diabetes Ca (2)':
			skipNumber = RowsToSkip(filename,x)
			df8 = x1.parse(x,skiprows = skipNumber)
			df8['Measure'] = 'Comprehensive Diabetes Care - HbA1c Test'
			df8['ClaimsthroughDate'] =  claimsthroughdate
			if "Exclusion" not in df8.columns:
				df8["Exclusion"] = ""
				
		if x[2:] in 'D-Comprehensive Diabetes Ca (3)':	
			skipNumber = RowsToSkip(filename,x)
			df9 = x1.parse(x,skiprows = skipNumber)
			df9['Measure'] = 'Comprehensive Diabetes Care - HbA1c control < 8.0'
			df9['ClaimsthroughDate'] =  claimsthroughdate
			if "Exclusion" not in df9.columns:
				df9["Exclusion"] = ""
				
		if x[2:] in 'D-Medication Management for Peo':
			skipNumber = RowsToSkip(filename,x)
			df10 = x1.parse(x,skiprows = skipNumber)
			df10['Measure'] = 'Medication Management for People With Asthma'
			df10['ClaimsthroughDate'] =  claimsthroughdate
			if "Exclusion" not in df10.columns:
				df10["Exclusion"] = ""
				
		if x[2:] in 'D-Plan All-Cause Readmissions':
			skipNumber = RowsToSkip(filename,x)
			df11 = x1.parse(x,skiprows = skipNumber)
			df11['Measure'] = 'Plan All-Cause Readmissions'
			df11['ClaimsthroughDate'] =  claimsthroughdate
			if "Exclusion" not in df11.columns:
				df11["Exclusion"] = ""
				
		if x[2:] in 'D-Use of Imaging Studies for Lo':
			skipNumber = RowsToSkip(filename,x)
			df12 = x1.parse(x,skiprows = skipNumber)
			df12['Measure'] = 'Use of Imaging Studies for Low Back Pain'
			df12['ClaimsthroughDate'] =  claimsthroughdate
			if "Exclusion" not in df12.columns:
				df12["Exclusion"] = ""
		
	#Writing to result file
	companyResult = df.append([df1, df2, df3,df4,df5,df6,df7,df8,df9,df10,df12])
	companyResult["Company"] = Company
	companyResult["DateReceived"] = CurrentTime
	companyResult["FileName"] = filename
	companyResult["DataSource"] = 'BCBS'
	
	
	#Drop hidden columns in Excel worksheet from result
	for x in companyResult.columns:
		if 'Unnamed' in x:
			companyResult = companyResult.drop(x,1)
	
	print len(companyResult)
	#Result file will go to pipe file
	companyResult.to_csv(extractFile, sep='|',index = False)
	
	#Debug Excel
	#resultFile = "Test" + Company + CurrentTime + ".xlsx"
	#writer = pd.ExcelWriter(resultFile)
	#companyResult.to_excel(writer, sheet_name="Test")  
	#writer.save()

###Main Function
if __name__ == "__main__":
	parser = argparse.ArgumentParser()
	parser.add_argument("--fileLocation")
	parser.add_argument("--newLocation")
	args = parser.parse_args()
	workbook = args.fileLocation
	extractFile = args.newLocation
	
	###Clean and produce extractFile
	CleanBCBSFile(workbook,extractFile)
		
