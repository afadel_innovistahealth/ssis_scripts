############################################################################
####                                                                    ####
#### Script Name: ACOHealthSolutions_MSSP_Quality_Preprocessor.rb       ####
#### Useage: ruby ACOHealth...Preprocessor.rb [input_file]              ####
#### Created: 08/06/2017 by AFF                                         ####   
####                                                                    ####
####                                                                    ####
#### Description: Adds filename column and data and filesourceDate col  ####
####                                                                    ####
####                                                                    ####
####                                                                    ####
####                                                                    ####
####                                                                    ####
############################################################################

require 'rubygems'
require 'Date'
require 'CSV'
require 'unidecoder'

#script takes one argument
fullyQualifiedFile =  ARGV[0].to_s 
p fullyQualifiedFile

def main(fullyQualifiedFile)
    
    tsStart = Time.now
    outputFilename = fullyQualifiedFile + '_processed.txt' #ARGV[1]
    tempCsv = File.read(fullyQualifiedFile); nil
	#tempCsv = tempCsv.encode("Windows-1252", invalid: :replace, undef: :replace).gsub('???','')
	#tempCsv = tempCsv.to_ascii
    
    #check for redundant quotations and remove them if present
    begin
    rawCSV = CSV.parse(tempCsv,:headers=>true,:col_sep=>'|');nil
    end
    
    #parse filedate from filename for metadata
    sourceFileDate = Date.parse(File.basename(fullyQualifiedFile)[/[0-9]{8}/]).strftime('%x')
    
    #create an array from the original file and add in metadata columns
    formattedCSV=[]; 
    rawCSV.each do |r| 
        r["SourceFile"] = outputFilename.encode("IBM437")
        r["SourceFileDate"] = sourceFileDate.encode("IBM437")
        r["DataSource"] = 'ACOHealthSolutions'.encode("IBM437")
        formattedCSV << r.to_hash
    end
    
    #collect the new headers
    newHeaders = []; formattedCSV[0].each {|k,v| newHeaders << k};nil
    
    #write pipe-delimited output file with new headers
    #CSV.open(outputFilename,'a',:encoding=>"UTF-8",:row_sep=>"\n",:col_sep=>'|',:write_headers=>true,:headers=>newHeaders) do |temp| 
    CSV.open(outputFilename,'a',:col_sep=>'|',:write_headers=>true,:headers=>newHeaders) do |temp| 
        formattedCSV.each do |record|
            row = []; record.each {|k,v| row << v}
            temp << row
        end
    end
    
    #recodeCsv = File.read(outputFilename)
    
    p (Time.now - tsStart).to_s + ' script runtime'
    p outputFilename.encoding
end

main(fullyQualifiedFile)
