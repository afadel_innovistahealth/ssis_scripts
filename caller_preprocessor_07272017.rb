############################################################################
####                                                                    ####
#### Script Name: caller_preprocessor.rb                                ####
#### Useage: ruby fonality_preprocessor.rb [input_file]                 ####
#### Created: 07/01/2017 by AFF                                         ####   
#### Modified: 7/26 -added XLS handling                                 ####
####                                                                    ####
#### Description: Preprocessor script to remove extra double-quotes     ####
####              and add metadata columns to the record for downstream ####
####              processing.                                           ####
####                                                                    ####
####                                                                    ####
####                                                                    ####
############################################################################

require 'rubygems'
require 'Date'
require 'roo-xls'

#script takes one argument
filename =  ARGV[0].to_s 
p filename

def main(filename)
    #filename = 'ABN 20170501.csv'
    #set output filename to input + processed suffix, which SSIS searches for

    excel = Roo::Excel.new(filename)
    tempCsv = excel.to_csv
    worksheetName = excel.sheets[0].to_s
    output_filename = filename + '_processed.txt' #ARGV[1]
    
    #check for redundant quotations and remove them if present
    begin
    csv_text = CSV.parse(tempCsv,:headers=>true,:col_sep=>',')
    rescue
        p 'in rescue block: '+tempCsv
		if tempCsv.include?('\\')
			tempCsv = tempCsv.gsub('\\','\\\\')
		end
         csv_text = tempCsv.gsub('\\"','\\').to_s
    end

    #if file does not require additional processing to remove quotes then process as normal
    unless csv_text.class == CSV::Table
        begin
        working_csv = CSV.parse(csv_text,:headers=>true,:col_sep=>',')
        rescue StandardError  => e #check for bad quotes in file and remove
            p e
        end
        else working_csv = csv_text
     end
     
    ##keep headers consistent between ABN and COM files for SSIS consumption
    #columns difference between COM and ABN file
    additional_headers = ["Duration","Agent","In","Out"]
    headers_to_add = []
    additional_headers.each do |a_h| 
        unless working_csv.headers.include? a_h; 
            headers_to_add << a_h
        end 
    end
    
    ##define metadata columns
    #check if file is for completed or abandoned calls and set a variable
    calltype = if worksheetName.include? 'Completed' 
            then '0' 
        elsif worksheetName.include? 'Abandoned' 
            then '1'; 
        else nil 
    end

    #parse filedate from filename for metadata
    sourcefiledate = begin	
						Date.parse(File.basename(filename)[/[0-9]{4}-[0-9]{2}-[0-9]{2}/]).strftime('%x')
					 rescue
						Date.parse(File.basename(filename)[/[0-9]{8}/]).strftime('%x')
					 end
    
    #create an array from the original file and add in metadata columns
    formatted_csv=[]; 
    working_csv.each do |r| 
        new_row = r.to_hash
        r["Abandoned"] = calltype
        r["SourceFilename"] = filename
        r["SourceFileDate"] = sourcefiledate
        r["DataSource"] = 'FONALITY'
        headers_to_add.each {|a_h| r[a_h] = nil; };nil
        formatted_csv << r.to_hash
    end
    
    #sort the columns so they are consistant between COM and ABN files, otherwise SSIS breaks 
    formatted_csv.map! {|r| r.sort }
    
    #collect the new headers, make the value 'Hold Time' consistent between COM and ABN files
    new_headers = []; formatted_csv[0].each {|k,v| new_headers << k};nil
    new_headers[new_headers.index('Hold')] = 'Hold Time' unless new_headers.include? 'Hold Time'
    
    #write pipe-delimited output file with new headers
    CSV.open(output_filename,'a',:col_sep=>'|',:write_headers=>true,:headers=>new_headers) do |temp| 
        formatted_csv.each do |record|
            row = []; record.each {|k,v| row << v}
            temp << row
        end
    end
end

main(filename)
