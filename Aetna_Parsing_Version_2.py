import pandas as pd
import os
import os.path
import sys
import shutil
import time
import numpy as np
import openpyxl as pyxl
import argparse

def GetCleanPath():
	Workingpath = str(sys.path[0])
	Workingpath = Workingpath.replace("\\", "//")
	return Workingpath

def GetQualFile(workbook):	
	filename = workbook
	###Debug hardcoded
	#filename = "AetnaQualityFile.xlsx"
	
	x1 = pd.ExcelFile(filename)

	qualSheet = ""
	
	###Excel loop through sheet names
	for x in x1.sheet_names:
		if "Quality" and "Detail" in x:
			print x
			qualSheet = x
			break
	###Return df
	df = x1.parse(qualSheet)
	return df

def getClaimsThroughDate(workbook):		
	#Read the workbook and worksheet name
	wbName = workbook
	###Debug hardcoded
	#wbName = "AetnaQualityFile.xlsx"
	
	###This is the Quality Definitions sheet
	x = "Quality - Report Definitions"
	
	wb = pyxl.load_workbook(wbName)
	ws = wb[x]
	
	claimDict = {}
	
	#valueList = ['Acei Arb Adherence','Statin Use in Diabetics','Breast Cancer Screening Compliance','Diabetes Controlled HbA1c','Diabetes Eye Exam']
	
	valueList = ['Acei Arb Adherence','Statin Use In Diabetics','Breast Screening Compliance','Colorectal Screening Compliance','Diabetes Controlled HbA1c','Diabetes Eye Exam']
	
	matchedList = ['Medication Reconciliation Post Discharge','Diabetes Medication Adherence','High Risk Medication','Statin Medication Adherence']
	
	#matchedList = ['Medication Reconciliation Post Discharge','Diabetes Medication Adherence','High Risk Medication','Statin Medication Adherence','Colorectal Cancer Screening']
	
	for x in range(70):
		if x != 0:
			valCheck = ws['A' + str(x)].value
			if valCheck in matchedList:
				print valCheck
				###make matched pairs for the lists we have
				###Add these to dictionary
				claimDict[valCheck] = ws['E' + str(x)].value
			else:
				if valCheck == "ACEI/ARB Medication Adherence":
					MatchVal = 'Acei Arb Adherence'
					claimDict[MatchVal] = ws['E' + str(x)].value
				elif valCheck == "Statin use in Diabetics":
					MatchVal = "Statin Use In Diabetics"
					claimDict[MatchVal] = ws['E' + str(x)].value
				elif valCheck == "Breast Cancer Screening":
					MatchVal = "Breast Screening Compliance"
					claimDict[MatchVal] = ws['E' + str(x)].value
				elif valCheck == "Diabetes Care -  Controlled HbA1c":
					MatchVal = "Diabetes Controlled HbA1c" 
					claimDict[MatchVal] = ws['E' + str(x)].value
				elif valCheck == "Diabetes Care - Eye Exams":
					MatchVal = "Diabetes Eye Exam"
					claimDict[MatchVal] = ws['E' + str(x)].value
				elif valCheck == "Colorectal Cancer Screening":
					MatchVal = "Colorectal Screening Compliance"
					claimDict[MatchVal] = ws['E' + str(x)].value
		#Debug Dictionary print	
		#print claimDict.items()
		###loop through list of all values 

	###Return to dataframe
	df = pd.DataFrame(claimDict.items(), columns=['Measure', 'ClaimsThroughDate'])
	
	#Debug Excel File
	#resultFile = "claimsDates04" + ".xlsx"
	#writer = pd.ExcelWriter(resultFile)
	#df.to_excel(writer, sheet_name="Test",index = False)  
	#writer.save()	
	
	return df
		
###Needs to accept dataframe
def CleanAetnaFile(dfHold,workbook,extractFile):	
    
	MeasureList = ['Acei Arb Adherence','Medication Reconciliation Post Discharge','Diabetes Medication Adherence','High Risk Medication','Statin Medication Adherence','Statin Use In Diabetics','Breast Screening Compliance','Colorectal Screening Compliance','Diabetes Controlled HbA1c','Diabetes Eye Exam']
	
	###Get DF from the GetQualFile Function	
	df = dfHold 
	###Strip New line from ACEI/ARB
	columnList = list(df)
	
	for x in columnList:
		if x.startswith('Acei') and x.endswith('Adherence'):
			df = df.rename(columns = {x:'Acei Arb Adherence'})
	
	###Begin unpivoting the dataframe
	df = pd.melt(df, id_vars=['Member ID','Member First Name','Member Last Name','Member DOB','Member Gender','TIN Name','Provider Name','NPI','Provider Group Name','Contract Number'], value_vars = MeasureList,
			var_name = 'Measure' , value_name = 'MeasureFlag')

	#Debug Excel		
	#resultFile = "AetnaAfterMelt" + ".xlsx"
	#writer = pd.ExcelWriter(resultFile)
	#df.to_excel(writer, sheet_name="Test",index = False)  
	#writer.save()	
	
	###Begin Parsing empty string values
	df = df.dropna(subset=['MeasureFlag'])
	
	###Begin Data Split for dates
	df = df.join(df['MeasureFlag'].str.split('-', 1, expand=True).rename(columns={0:'MeasureFlag_2', 1:'MeasureDate'}))
	
	###Drop holder from DF split
	df = df.drop('MeasureFlag', 1)
	
	###Rename Measure_2 column
	df = df.rename(columns = {'MeasureFlag_2':'MeasureFlag'})
    
	###Get Claims Through Date 
	
	dfClaims = getClaimsThroughDate(workbook)
	
	###Return DF Back to ExcelFile
	
	result = pd.merge(df, dfClaims,how = 'left', on = 'Measure')
	
	#Adding Columns to DF
	result["DataSource"] = "Aetna" 
	result["SourceFile"] = workbook
	
	#Move result df to csv file
	result.to_csv(extractFile, sep='|',index = False)
		
	###Debug Excel file
	#resultFile = "AetnaTESTT04Fileable" + ".xlsx"
	#writer = pd.ExcelWriter(resultFile)
	#result.to_excel(writer, sheet_name="Test",index = False)  
	#writer.save()	

###Main Function
if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--fileLocation")
    parser.add_argument("--newLocation")
    args = parser.parse_args()
    #print args.fileLocation
    #print args.newLocation
    workbook = args.fileLocation
    extractFile = args.newLocation
    dfAetna = GetQualFile(workbook)
    dfAetnaFinal = CleanAetnaFile(dfAetna,workbook,extractFile)
    #exit(0)